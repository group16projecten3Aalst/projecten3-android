package com.example.shmoopsy.evachallengeapp;

import android.app.Activity;
import android.app.Dialog;
import android.content.Intent;
import android.graphics.Typeface;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ImageView;
import android.widget.TextView;

public class InfoActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_info);
        TextView txtTitle = (TextView) findViewById(R.id.txtTitle);
        Typeface typeFaceTitle=Typeface.createFromAsset(getAssets(),"fonts/Museo900Regular.otf");
        txtTitle.setTypeface(typeFaceTitle);
        TextView txtEvaInfo = (TextView) findViewById(R.id.txtEvaInfo);
        Typeface typeFaceInfo=Typeface.createFromAsset(getApplicationContext().getAssets(),"fonts/Museo300-Regular.otf");
        txtEvaInfo.setTypeface(typeFaceInfo);

    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_info, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_login) {
            DialogFragment dialog = new LoginDialogFragment();

            //setting custom layout to dialog
            dialog.show(getSupportFragmentManager(), "login");
        }
        if (id == R.id.action_logout) {
            return true;
        }
        if (id == R.id.action_account) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

}
